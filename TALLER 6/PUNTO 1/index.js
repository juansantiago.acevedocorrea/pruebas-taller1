var {
    defineSupportCode
} = require('cucumber');
var {
    expect
} = require('chai');

defineSupportCode(({
    Given,
    When,
    Then
}) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('/');
        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');
    });

    When('I fill a wrong email and password', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123467891')
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.element('button=Ingresar').click()
    });

    Then('I expect to not be able to login', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    });

    When(/^I fill with (.*) and (.*)$/, (email, password) => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys(email);

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password)
    });

    Then('I expect to see {string}', error => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).to.include(error);
    });

    When('I fill with correct credentials', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('pruebas@me.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('miso4208')
    });

    Then('I expect to see login', error => {
        browser.waitForExist('#cuenta', 5000);
    });

    // cy.visit('https://losestudiantes.co')
    // //             cy.contains('Cerrar').click()
    // //             cy.wait(1000)
    // //             cy.contains('Ingresar').click()
    // //             cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Daniel")
    // //             cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Soto")
    // //             cy.get('.cajaSignUp').find('input[name="correo"]').click().type('dmisoto' + id + '@me.co')
    // //             cy.get('.cajaSignUp').find('input[name="password"]').click().type("miso123456")
    // //             cy.get('.cajaSignUp').find('select[name="idUniversidad"]').select("Universidad de los Andes")
    // //             cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select("Administración")
    // //             cy.get('.cajaSignUp').find('input[name="acepta"]').click()
    // //             cy.get('.cajaSignUp').contains('Registrarse').click()
    // //             cy.contains('Registro exitoso!')

});