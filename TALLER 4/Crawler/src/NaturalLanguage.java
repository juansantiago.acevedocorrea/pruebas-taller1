
import com.google.cloud.language.v1.AnalyzeEntitiesRequest;
import com.google.cloud.language.v1.AnalyzeEntitiesResponse;
import com.google.cloud.language.v1.Document;
import com.google.cloud.language.v1.Document.Type;
import com.google.cloud.language.v1.EncodingType;
import com.google.cloud.language.v1.Entity;
import com.google.cloud.language.v1.LanguageServiceClient;
import com.google.cloud.language.v1.Sentiment;


import java.io.IOException;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.compute.Compute;


/**
 * @author gusal
 * Para definir la variable de entorno del archivo de autenticacion(Carpeta credentials):
 *  MAC: export GOOGLE_APPLICATION_CREDENTIALS=<path_to_service_account_file>
 *  WIN: set GOOGLE_APPLICATION_CREDENTIALS=<path_to_service_account_file>
 *  y reiniciar el compu
 */

public class NaturalLanguage {
	
  public static void main(String... args) throws Exception {
	  
	  
    // Instantiates a client
    LanguageServiceClient language = LanguageServiceClient.create();

    // The text to analyze
    String text = "Hello, world!";
    Document doc = Document.newBuilder()
            .setContent(text).setType(Type.PLAIN_TEXT).build();

    // Detects the sentiment of the text
    Sentiment sentiment = language.analyzeSentiment(doc).getDocumentSentiment();

    System.out.printf("Text: %s%n", text);
    System.out.printf("Sentiment: %s, %s%n", sentiment.getScore(), sentiment.getMagnitude());
  }
  
  
  public static String processSentiments(String txt) throws IOException{
	  
	    LanguageServiceClient language = LanguageServiceClient.create();
	    Document doc = Document.newBuilder()
	            .setContent(txt).setType(Type.PLAIN_TEXT).build();

	    // Detects the sentiment of the text
	    Sentiment sentiment = language.analyzeSentiment(doc).getDocumentSentiment();
	    
	    AnalyzeEntitiesRequest request = AnalyzeEntitiesRequest.newBuilder()
	    	      .setDocument(doc)
	    	      .setEncodingType(EncodingType.UTF16).build();
	    
	    AnalyzeEntitiesResponse response = language.analyzeEntities(request);
	    
	    for(Entity str:response.getEntitiesList()){
	    		System.out.println(str.toString());
	    }
	    
	    System.out.printf("Text: %s%n", txt);
	    System.out.printf("Sentiment: %s, %s%n", sentiment.getScore(), sentiment.getMagnitude());
		return Float.toString(sentiment.getScore()).replace(',', '.');
  }
  
  
}
