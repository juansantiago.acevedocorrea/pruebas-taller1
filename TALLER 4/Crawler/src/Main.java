import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {

	public static String PLAY_CATEGORIES = "https://play.google.com/store/apps/category/";


	/**
	 * Metodo main que maneja la interfaz en consola
	 * @param args
	 * @throws IOException Cuando hay problemas con el input del usuario
	 */
	public static void main(String[] args) throws IOException {

		//Seleccionar mercado
		String mercado = "";
		System.out.println("Presione enter para hacer una búsqueda sobre el Play Store de Google.");
		try{
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			bufferRead.readLine();
		}
		catch (IOException e) {
			System.out.println("Numero invalido");
		}

		//Imprimir categorias
		List<String> categorias = buscarCategorias();
		for(int i = 0; i < categorias.size(); i=i+3){
			System.out.println("[" + i + "]" + " " + categorias.get(i+1) + "\t\t\t" + "[" + (i+1) + "]" + " " + categorias.get(i+2) + "\t\t\t" + "[" + (i+2) + "]" + " " + categorias.get(i+2) + "\n");
		}
		System.out.println("Seleccione una categoria de las anteriores:\n");

		//Seleccionar categoria
		String categoria = "";
		try{
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			String input = bufferRead.readLine();
			categoria = categorias.get(Integer.parseInt(input));
		}
		catch (IOException e) {
			System.out.println("Numero invalido");
		}

		//Imprimir posibles reportes
		String reporte = "";
		System.out.println("Seleccione un tipo de reporte:" + "\n\n" + "[1] Descripciones" + "\n" + "[2] Comentarios\n");
		try{
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			reporte = bufferRead.readLine();
		}
		catch (IOException e) {
			System.out.println("Numero invalido");
		}

		//Generar reporte
		if(generarReporte(mercado, categoria, reporte) )
			System.out.println("Reporte generado exitosamente\n");
		else
			System.out.println("El reporte no pudo ser generado\n");
	}


	/**
	 * Metodo que busca las categorias del mercado seleccionado por el susuario
	 * @param inputString El mercado seleccionado
	 * @return Una lista con las categorias en String
	 * @throws IOException 
	 */
	private static List<String> buscarCategorias() throws IOException {
		
		Document doc = Jsoup.connect("https://42matters.com/docs/app-market-data/android/apps/google-play-categories").timeout(0).get();
		List<String> hrefs = new ArrayList<String>();
		String href = null;
		Elements anchors = doc.getElementsByClass("card-click-target");
		anchors = doc.getElementsByTag("tr");
		for (Element element:anchors) {
			if(element.select("td").first() != null){
				href = element.select("td").first().text().toString();
			}
			hrefs.add(href);
		}
		return hrefs;
	}

	/**
	 * Metodo que genera un archivo con el reporte solicitado por el usuario
	 * @param mercado El mercado seleccionado
	 * @param categoria La categoria seleccionada
	 * @param reporte El tipo de reporte que se quiere generar
	 * @return
	 * @throws IOException 
	 */
	private static boolean generarReporte(String mercado, String categoria, String reporte) throws IOException{

		Document doc = Jsoup.connect(PLAY_CATEGORIES + categoria + "/collection/topselling_paid").timeout(0).get();
		Document detailDoc = null;

		HashSet<String> hrefs = new HashSet<String>();
		String href = null;

		Elements anchors = doc.getElementsByClass("card-click-target");

		for (Element element:anchors) {
			href = "https://play.google.com/" + element.attr("href").toString();
			hrefs.add(href);
		}

		List<String> descriptions = new ArrayList<String>();
		List<String> comentarios = new ArrayList<String>();
		System.out.println("Descargando desde el Marketplace...");

		int i = 0;
		for(String url:hrefs) {
			if (i > 10) break;
			i++;
			detailDoc = Jsoup.connect(url).timeout(0).get();
			try{
				String description = detailDoc.select("[itemprop='description']").text().substring(0, 200);
				String comentario = detailDoc.select("[class='review-text']").text().substring(0, 200);
				descriptions.add(description);
				comentarios.add(comentario);
			}catch (Exception e) {
				System.out.println("...");
			}
		}

		HashMap<String, String> descripcionesMap = new HashMap<>();
		HashMap<String, String> comentariosMap = new HashMap<>();
		
		if (reporte.equals("1")) {
			descriptions.forEach(item -> {
				try {
					System.out.println("Procesando el lenguaje...");
					descripcionesMap.put(item, NaturalLanguage.processSentiments(item));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			generarReporte(descripcionesMap);
		} else if (reporte.equals("2")) {
			comentarios.forEach(item -> {
				try {
					System.out.println("Procesando el lenguaje...");
					comentariosMap.put(item,  NaturalLanguage.processSentiments(item));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			generarReporte(comentariosMap);
		}
		return true;
	}
	
	private static void generarReporte( HashMap<String, String> descripciones) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("reporte.csv"));
		StringBuilder sb = new StringBuilder();
		sb.append("Comentario o descripcion,Sentimiento" + '\n');
		
		
		ArrayList<String> llaves = new ArrayList<String>(descripciones.keySet());
		for(int i = 0; i < descripciones.size(); i++){
			sb.append(llaves.get(i).replaceAll(",", "") + ",");
			sb.append(descripciones.get(llaves.get(i)));
			sb.append('\n');
		}
		
		pw.write(sb.toString());
		pw.close();
		System.out.println("Reporte generado, refresque el árbol de paquetes para visualizarlo.");
		
	}

}