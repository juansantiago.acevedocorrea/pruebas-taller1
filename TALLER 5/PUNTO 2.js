describe('Prueba Random', function() {
    it('Prueba Monkey', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.wait(1000)
        cy.contains('Ingresar').click()

        //Login exitoso
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("juanme@me.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123123123")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        monkey(10000)
    })
})

function monkey(left) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var left = left;
    if (left > 0) {
        let eventType = getRandomInt(1, 4);
        if (eventType == 1) {
            cy.document().then(function (document) {
                if (document.getElementsByTagName("a").length > 0) {
                    cy.get('a').then($links => {
                        var link = $links.get(getRandomInt(0, $links.length));

                        if (!Cypress.Dom.isHidden(link) && !link.host.includes(".com")) {
                            cy.wrap(link).click({
                                force: true
                            });
                        }
                    });
                }
            })
        } else if (eventType == 2) {
            cy.document().then(function (document) {
                if (document.getElementsByTagName("input").length > 0) {
                    cy.get('input[type="text"]').then($inputs => {
                        var input = $inputs.get(getRandomInt(0, $inputs.length));
                        if (!Cypress.Dom.isHidden(input)) {
                            cy.wrap(input).click({
                                force: true
                            }).type('q;oinefowqnfoifqeno1235', {
                                force: true
                            });
                        }
                    });
                }
            })
        } else if (eventType == 3) {
            cy.document().then(function (document) {
                if (document.getElementsByTagName("select").length > 0) {
                    cy.get('select').then($combos => {
                        var combo = $combos.get(getRandomInt(0, $combos.length));
                        if (!Cypress.Dom.isHidden(combo)) {
                            let options = combo.children
                            let opt = getRandomInt(0, options.length)
                            cy.wrap(combo).select(options[opt].value, {
                                force: true
                            });
                        }
                    });
                }
            })
        } else {
            cy.document().then(function (document) {
                if (document.getElementsByTagName("button").length > 0) {
                    cy.get('button').then($buttons => {
                        var button = $buttons.get(getRandomInt(0, $buttons.length));
                        if (!Cypress.Dom.isHidden(button)) {
                            cy.wrap(button).click({
                                force: true
                            });
                        }
                    });
                }
            })
        }
        setTimeout(monkey, 2000, left - 1);
    }
}